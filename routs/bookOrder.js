const router = require("express").Router();
const bookOrder =require('../apiservice/bookorder');


router.get("/bookorderList",bookOrder.getOrder);

router.get("/bookorderList/:bookId",bookOrder.getspesific);

router.delete("/bookorderList/delete/:bookId",async (req,res)=>{
    await bookOrder.deleteorder(req,res)
});

router.post("/bookorderList",bookOrder.postOrder);

router.post("/bookorderList/:bookId",bookOrder.postSpesific);

module.exports = router;