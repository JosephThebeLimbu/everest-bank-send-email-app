
var nodemailer = require('nodemailer');
var hbs = require('nodemailer-express-handlebars');

const Contactmail = async (req,res)=>{
            function submission(data){
                let transporter = nodemailer.createTransport({
                    host: process.env.SMTP_HOST,
                    secure: false,
                    port: process.env.SMTP_PORT,
                auth: {
                        user:process.env.EMAIL ,
                        pass: process.env.PASSWORD
                    }
                });
                transporter.use('compile', hbs({
                viewEngine:"express-handlebars",
                viewPath:"./"
                }));
                let from = `${data.email}<${process.env.EMAIL}>`;
                let HelperOptions = {
                    from: from,
                    to: process.env.Send_Contact,
                    replyTo:data.email,
                    subject: `${process.env.Subject_Contact}`,
                    template:"main",
                    context: {
                    subject:`${process.env.body_contact} :`,
                    name:data.name,
                    email:data.email,
                    mobile:data.phone,
                    message:data.message
                    }
                };

                if(req.headers.auth === process.env.salt_key){
                    console.log("you have matching salt key",req.headers.auth);
                    transporter.sendMail(HelperOptions, (error, info) => {            
                    if (error) {
                        console.log("mailconfig",error)
                    }
                    else {
                    res.status(200).json(HelperOptions);
                    }
                })
                }
                else{
                    res.send("Error");
                }        
            }
     await submission(req.body);

}
module.exports=Contactmail