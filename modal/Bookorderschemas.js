const mongoose = require('mongoose');

const BookOrderSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        default: null
    },
    email: {
        type: String,
        required: true
    },
    order_Date: {
        type: String,
        required: true
    },
    time: {
        type: String,
        required: true
    },
    person: {
        type: Number,
        default: null
    },
    requirement: {
        type: String,
    }
});

module.exports = mongoose.model('BookOrder', BookOrderSchema);