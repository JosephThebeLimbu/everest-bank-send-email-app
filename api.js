const express=require('express');
// mongo connection
require("./modal/db.connect");
const mongoose = require("mongoose");
const bodyparser = require('body-parser');
var cors = require('cors');
require('dotenv').config();
var app = express();
//middileware
app.use(bodyparser.urlencoded({
    extended: true
}));
app.use(bodyparser.json());
app.use(cors()) ;
app.use(express.static("public"));
// used morgen for logger
const morgan = require('morgan');
app.use(morgan('dev'));
mongoose.set("useFindAndModify", false);

//service
const MailController =require('./service/MailController');
const Contactmail=require('./service/ContactMail');
const SuscriberController=require('./service/SuscriberController');
app.post("/sendmail/everestSpices",MailController);
app.post("/sendmail/everestSpices/contactform",Contactmail);
app.use("/sendmail/everestSpices/suscriber",SuscriberController);
app.use("/bookorder", require("./routs/bookOrder"));

let PORT=process.env.PORT||3060
app.listen(PORT,() => {
    console.log('Express server started at port : 3060');
});