const mongoose = require("mongoose");
const BookOrder = mongoose.model("BookOrder");
const bookOrder = {
    getOrder: async(req, res) => {
        let limit = parseInt(req.query.limit);
        let skip = (parseInt(req.query.page) - 1) * parseInt(limit);
        console.log(skip);
        let data = await BookOrder.find({})
            .sort({ _id: -1 })
            .limit(limit)
            .skip(skip)
            .exec(function(err, docs) {
                if (err) {
                    res.send("error while fetching");
                }
                console.log(docs.length);
                res.send(docs);
            });
    },

    getspesific: async(req, res) => {
        let bookId = req.params.bookId;
        let data = await BookOrder.findById({ _id: bookId });
        if (data) {
            res.send(data);
        } else {
            res.send("Not found book order");
        }
    },

    deleteorder: async(req, res) => {
        let bookId = req.params.bookId;
        await BookOrder.findOneAndRemove({ _id: bookId }, (err, result) => {
            if (err) {
                res.status(404).send(err);
            } else {
                res.status(200).send(result);
            }
        });
    },

    postOrder: async(req, res) => {
        let postOrder = {
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone,
            order_Date: req.body.order_Date,
            time: req.body.time,
            person: req.body.person,
            requirement: req.body.requirement,
        };
        let order = BookOrder(postOrder);
        await order.save().then((err, docs) => {
            if (err) {
                res.send(err);
            }
            res.send(docs);
        });
    },

    postSpesific: async(req, res) => {
        let bookId = req.params.bookId;
        let postOrder = {
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone,
            order_Date: req.body.order_Date,
            time: req.body.time,
            person: req.body.person,
            requirement: req.body.requirement,
        };

        let data = await BookOrder.findOneAndUpdate({ _id: bookId },
            postOrder,
            function(err, docs) {
                if (err) {
                    res.send(err);
                }
                res.send(docs);
            }
        );
    },
};
module.exports = bookOrder;